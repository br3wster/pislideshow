from tkinter import *
from PIL import Image, ImageTk, ImageEnhance
import random
import subprocess
import time

WIDTH = 1920
HEIGHT = 1080
TRANSITION_DELAY = 50
SLIDE_DELAY = 5000
STEPS = 8
RANDOMIZE = False
ratio = WIDTH / HEIGHT

class Slideshow(Tk):
    def __init__(self, files):
        self.files = files
        self.transition = STEPS
        self.index = 0
        self.resized = False
        Tk.__init__(self)
        self.config(cursor = "none")
        self.attributes("-fullscreen", True)
        frame = Frame(self)
        frame.pack()
        self.canvas = Canvas(frame, height = HEIGHT, width = WIDTH)
        self.canvas.pack()
        self.black = Image.open("black.jpg")
        self.tkpi = ImageTk.PhotoImage(self.black)
        self.slide = Label(self, image = self.tkpi)
        self.slide.place(x = 0, y = 0, width = self.black.size[0], height = self.black.size[1])
        self.transition_light()

    def transition_dark(self):
        if self.transition < STEPS:
            self.transition += 1
            self.transition_slide = ImageEnhance.Brightness(self.slide_image).enhance(1.0 - (self.transition / STEPS))
            self.tkpi = ImageTk.PhotoImage(self.transition_slide)
            self.slide.configure(bg = "black", image = self.tkpi)
            self.slide.image = self.tkpi
            self.after(TRANSITION_DELAY, self.transition_dark)
        else:
            self.after(TRANSITION_DELAY, self.transition_light)

    def transition_light(self):
        self.resized = False

        if self.transition == STEPS:
            if RANDOMIZE:
                self.index = random.randint(0, len(self.files) - 1)
            else:
                self.index += 1
                self.index %= len(self.files)

            self.slide_image = Image.open(self.files[self.index])

            image_ratio = self.slide_image.size[0] / self.slide_image.size[1]

            if image_ratio > ratio:
                # This means it is wider. So scale by width.
                percent = (WIDTH / float(self.slide_image.size[0]))
                hsize = int((float(self.slide_image.size[1]) * float(percent)))

                if WIDTH != self.slide_image.size[0]:
                    self.resized = True
                    print("Size comparison of %s: (Height) %d -> %d (Width) %d -> %d" % (self.files[self.index], self.slide_image.size[1], HEIGHT, self.slide_image.size[0], WIDTH))
                    self.slide_image = self.slide_image.resize((WIDTH, hsize), Image.ANTIALIAS)

            else:
                # This means it is taller or equal in ratio. So scale by height.
                percent = (HEIGHT / float(self.slide_image.size[1]))
                wsize = int((float(self.slide_image.size[0]) * float(percent)))

                if HEIGHT != self.slide_image.size[1]:
                    self.resized = True
                    print("Size comparison of %s: (Height) %d -> %d (Width) %d -> %d" % (self.files[self.index], self.slide_image.size[1], HEIGHT, self.slide_image.size[0], WIDTH))
                    self.slide_image = self.slide_image.resize((wsize, HEIGHT), Image.ANTIALIAS)


            if self.resized:
                self.slide_image.save(self.files[self.index])

        if self.transition > 0:
            self.transition -= 1
            self.transition_slide = ImageEnhance.Brightness(self.slide_image).enhance(1.0 - (self.transition / STEPS))
            self.tkpi = ImageTk.PhotoImage(self.transition_slide)
            self.slide.configure(bg = "black", image = self.tkpi)
            self.slide.image = self.tkpi
            self.after(TRANSITION_DELAY, self.transition_light)
        else:
            self.after(TRANSITION_DELAY, self.slideshow)

    def slideshow(self):

        self.tkpi = ImageTk.PhotoImage(self.slide_image)
        self.slide.configure(bg = "black", image = self.tkpi)
        self.slide.image = self.tkpi
        self.after(SLIDE_DELAY, self.transition_dark)


process = subprocess.Popen("find /media -name '*' -exec file {} \; | grep -o -P '^.+: \w+ image' | sed \"s/:.*image.*//\"", shell=True, stdout=subprocess.PIPE)
process.wait()
result_lines = process.stdout.readlines()
slides = []

for result_line in result_lines:
    slides.append(result_line.decode("utf-8").strip())

if len(slides) > 0:
    root = Slideshow(slides)
    root.mainloop()
